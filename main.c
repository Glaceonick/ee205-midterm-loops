///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Midterm - Loops
///
/// @file    main.c
/// @version 1.0
///
/// Sums up values in arrays
///
/// Your task is to print out the sums of the 3 arrays...
///
/// The three arrays are held in a structure.  Consult numbers.h for the details.
///
/// For array1[], you'll iterate over using a for() loop.  The correct sum for 
/// array1[] is:  48723737032
/// 
/// For array2[], you'll iterate over it using a while()... loop.
///
/// For array3[], you'll iterate over it using a do ... while() loop.
///
/// Sample output:  
/// $ ./main
/// 11111111111
/// 22222222222
/// 33333333333
/// $
///
/// @brief Midterm - Loops - EE 205 - Spr 2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include "numbers.h"

int main() {
   // Sum array1[] with a for() loop and print the result
	int x;
	long sumx = 0;

	for (x = 0; x <= 100; x++) {
		sumx = sumx + threeArrays.array1[x];
	}
	printf("Sum of array 1: %d\n", sumx);

   // Sum array2[] with a while() { } loop and print the result
	int y;
	long sumy = 0;

	while (y <= 100) {
		sumy = sumy + threeArrays.array2[y];
		y++;
	}
	printf("Sum of array 2: %d\n", sumy);

   // Sum array3[] with a do { } while() loop and print the result
	int z;
	long sumz = 0;

	do {
		sumz = sumz + threeArrays.array3[z];
		z++;
	}
	while (z <= 100);

	printf("Sum of array 3: %d\n", sumz);

	return 0;
}

